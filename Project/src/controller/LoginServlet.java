package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		Object info = session.getAttribute("userInfo");

		//未ログイン
		if ( null != info ) {
		// トップページへ遷移(リダイレクト).
		response.sendRedirect("ListServlet");
		return;
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String Loginid = request.getParameter("Loginid");
		String password = request.getParameter("password");

		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(Loginid, password);

			if(user == null) {
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");

				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
				dispatcher.forward(request, response);
				return;
			}
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);

			response.sendRedirect("ListServlet");

	}

}
