package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	//新規登録ボタンを押した際にuserRegistration.jspに遷移
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションスコープにログイン情報があるか確認
		HttpSession session = request.getSession();

		Object info = session.getAttribute("userInfo");

		//未ログイン
		if ( null == info ) {
		// トップページへ遷移(リダイレクト).
		response.sendRedirect("LoginServlet");
		return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//登録画面での入力内容をリクエストスコープに格納
		request.setCharacterEncoding("UTF-8");
		String loginID = request.getParameter("loginID");
		String password = request.getParameter("password");
		String passwordre = request.getParameter("passwordre");
		String username = request.getParameter("username");
		String birthday = request.getParameter("birthday");

		//UserDaoインスタンス生成
		UserDao userDao = new UserDao();

		//ログインIDがデータテーブルに存在するか確認
		User user2 = userDao.idCheck(loginID);

		//入力内容の判断
		//入力内容に問題があればエラーメッセージを出し、登録画面に遷移
		//問題がなければ、ユーザ一覧画面に遷移
		//入力内容に漏れがあればエラー
		//空文字チェック※パスワード以外の入力内容は遷移先に引継ぐ
		if (loginID.equals("") || username.equals("") || birthday.equals("") || password.equals("")) {
			request.setAttribute("errMsg", "入力内容に漏れがあります");
			request.setAttribute("loginID", loginID);
			request.setAttribute("username", username);
			request.setAttribute("birthday", birthday);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

			//パスワードと確認が一致しなければエラー※パスワード以外の入力内容は遷移先に引継ぐ
		if (!password.equals(passwordre)) {
			request.setAttribute("errMsg", "パスワードが正しくありません");
			request.setAttribute("loginID", loginID);
			request.setAttribute("username", username);
			request.setAttribute("birthday", birthday);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (user2 != null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginID", loginID);
			request.setAttribute("username", username);
			request.setAttribute("birthday", birthday);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		}


		userDao.newReg(loginID, username, birthday, password);
		response.sendRedirect("ListServlet");

	}

}
