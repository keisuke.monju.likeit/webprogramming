package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		Object info = session.getAttribute("userInfo");

		//未ログイン
		if ( null == info ) {
		// トップページへ遷移(リダイレクト).
		response.sendRedirect( "LoginServlet" );
		return;
		}

		UserDao userDao = new UserDao();
		String id = request.getParameter("id");

		User user = userDao.UserUpdate(Integer.parseInt(id));
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 文字化け防止文
		request.setCharacterEncoding("UTF-8");

		//UserDaoインスタンス生成
		UserDao userDao = new UserDao();

		//更新用idを取得
		String id = request.getParameter("id");

		//idをイント型にキャストして該当idを削除
		userDao.UserDelete(Integer.parseInt(id));

		//削除完了後、リスト画面に遷移
		response.sendRedirect("ListServlet");
	}

}
