package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションスコープにログイン情報があるか確認
		HttpSession session = request.getSession();

		Object info = session.getAttribute("userInfo");

		//未ログイン
		if ( null == info ) {
		// トップページへ遷移(リダイレクト).
		response.sendRedirect("LoginServlet");
		return;
		}

		UserDao userDao = new UserDao();
		String id = request.getParameter("id");

		User user = userDao.UserUpdate(Integer.parseInt(id));
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 文字化け対策
		request.setCharacterEncoding("UTF-8");
		// daoインスタンス生成
		UserDao userDao = new UserDao();
		// 更新条件用主キー
		String id = request.getParameter("id");
		//入力された情報をリクエストパラメーターより取得
		String password = request.getParameter("password");
		String passwordre = request.getParameter("passwordre");
		String username = request.getParameter("username");
		String birthday = request.getParameter("birthday");

		// 空文字チェック
		if (username.equals("") || birthday.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			User user = userDao.UserUpdate(Integer.parseInt(id));
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}
		// パスワード一致チェック
		if (!password.equals(passwordre)) {
			request.setAttribute("errMsg", "パスワードが正しくありません");

			User user = userDao.UserUpdate(Integer.parseInt(id));
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		// パスワード含め更新
		if (password.equals("")) {
			// パスワード以外更新
			userDao.newNotPass(username, birthday, id);
		} else {
			// パスワード含め更新
			userDao.newPass(username, birthday, password, id);
		}
		// 更新後、リストに遷移
		response.sendRedirect("ListServlet");
		//		User users = userDaos.newNotPass(username, birthday, id);
		//		if (password.equals("") && passwordre.equals("") && (username.equals("") || birthday.equals(""))) {
		//			request.setAttribute("errMsg", "入力された内容は正しくありません");
		//
		//			response.sendRedirect("UpdateServlet");
		//			return;
		//
		//		}

		//		User users = userDaos.newNotPass(username, birthday, id);

		//		String source = "password";
		//		String source2 = "passwordre";
		//		Charset charset = StandardCharsets.UTF_8;
		//		String algorithm = "MD5";
		//
		//		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		//		String result = DatatypeConverter.printHexBinary(bytes);
		//
		//		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source2.getBytes(charset));
		//		String result = DatatypeConverter.printHexBinary(bytes);

		//		入力内容の確認　問題があればエラーを返す

		//		session.setAttribute("userInfo", users);
		//		response.sendRedirect("ListServlet");
		//
		//		if (!password.equals(passwordre)) {
		//			request.setAttribute("errMsg", "パスワードが正しくありません");
		//
		//			response.sendRedirect("UpdateServlet");
		//			return;
		//
		//		} else if (username.equals("") || birthday.equals("")) {
		//			request.setAttribute("errMsg", "入力された内容は正しくありません");
		//
		//			response.sendRedirect("UpdateServlet");
		//			return;
		//		}
		//		{
		//
		//			UserDao userDao = new UserDao();
		//			User user = userDao.newPass(username, birthday, password, id);
		//
		//			session.setAttribute("userInfo", user);
		//			response.sendRedirect("ListServlet");
		//
		//		}
	}

}
