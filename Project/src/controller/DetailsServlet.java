package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DetailsServlet
 */
@WebServlet("/DetailsServlet")
public class DetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		Object info = session.getAttribute("userInfo");

		//未ログイン
		if ( null == info ) {
		// トップページへ遷移(リダイレクト).
		response.sendRedirect("LoginServlet");
		return;
		}

		//UserDaoインスタンスを生成
		UserDao userDao = new UserDao();
		//getParameterよりIDの値を取得
		String id = request.getParameter("id");

		//user変数にリクエストスコープfindUserDateにidを数値化してデータの生成
		User user = userDao.findUserDate(Integer.parseInt(id));
		//リクエストスコープにuser変数に代入された値を保存
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDetails.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
