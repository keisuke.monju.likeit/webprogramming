<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>ユーザ情報詳細参照</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<div class="container text-align: right">${userInfo.name} さん
  				<form style="text-align: right" action="LogoutServlet"
			method="get">
			<input type="submit" class="btn btn-link" value="ログアウト">
				</form>
  		  </div>
	</header>

	<p style="text-align: center">
		<font size="6">ユーザ情報詳細参照</font>
	</p>

				<div >
					<p>ログインID:${user.loginId }</p>
					<p>ユーザ名:${user.name }</p>
					<p>生年月日:${user.birthDate }</p>
					<p>登録日時:${user.createDate }</p>
					<p>更新日時:${user.updateDate }</p>
				</div>


				<form style="text-align: right" action="ListServlet" method="get">
					<input type="submit" class="btn btn-link" value="戻る">
				</form>

</body>
</html>