<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>ユーザ情報更新</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<header>
            <div class="container text-align: right">${userInfo.name} さん
  				<form style="text-align: right" action="LogoutServlet"
			method="get">
			<input type="submit" class="btn btn-link" value="ログアウト">
				</form>
  		  </div>
	</header>



	<p style="text-align: center">
		<font size="6">ユーザ情報更新</font>
	</p>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<form class="form-horizontal container" action="UpdateServlet" method="post">
		<input type="hidden" name="id" value="${user.id}">
			<div class="form-group row">
				<label class="col-sm-2 control-label">ログインID</label>
				<div class="col-sm-10">
					${user.loginId }
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">パスワード</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" placeholder="Password" name=password>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" placeholder="Password(確認)" name=passwordre>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">ユーザ名</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" value=${user.name } name=username>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">生年月日</label>
				<div class="col-sm-10">
					<input class="form-control" type="date" value=${user.birthDate } name=birthday>
				</div>
			</div>

			<input type="submit" class="btn btn-info btn-block button-center" value="更新">
			</form>

			<div>
				<form style="text-align: right" action="ListServlet" method="get">
				<input type="submit" class="btn btn-link" value="戻る">
				</form>
			</div>




</body>
</html>