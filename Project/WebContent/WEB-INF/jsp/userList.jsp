<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>ユーザー一覧画面</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>
	<header>

		<div class="navbar-header text-center">
			<p class="display-4">ユーザ一覧</p>
		</div>

		<div class="container text-align: right">${userInfo.name}さん
			<form style="text-align: right" action="LogoutServlet" method="get">
				<input type="submit" class="btn btn-link" value="ログアウト">
			</form>
		</div>
	</header>

	<div class="container">

		<!-- //新規登録ボタンを押した際にRegistrationServletの -->
		<!-- //dogetメソッドを実行 -->
		<form style="text-align: right" action="RegistrationServlet" method="get">
			<input type="submit" class="btn btn-primary" value="新規登録">
		</form>


		<form class="form-horizontal" action="ListServlet" method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">ログインID</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" placeholder="LoginID" name=loginID>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">ユーザ名</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" placeholder="Username" name=username>
				</div>
			</div>
			<div class="form-group" id="datepicker-default">
				<label class="col-sm-3 control-label">生年月日</label>
				<div class="col-sm-9 form-inline">
					<input type="date" class="form-control" placeholder="yyyy年MM月dd日" name=birthday>
					<div class="input-group-addon">
						～
					<input type="date" class="form-control" placeholder="yyyy年MM月dd日" name=birthday2>
						<div class="input-group-addon"></div>
					</div>
				</div>
			</div>

			<div>
					<input type="submit" class="btn btn-primary btn-block" value="検索">
			</div>
		</form>
	</div>

	<hr>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<c:if test="${userInfo.name == '管理者'}">
							<td><a class="btn btn-primary"
								href="DetailsServlet?id=${user.id}">詳細</a> <a
								class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="DeleteServlet?id=${user.id}">削除</a>
							</td>
						</c:if>
						<c:if test="${userInfo.name != '管理者'}">
							<td><a class="btn btn-primary"
								href="DetailsServlet?id=${user.id}">詳細</a> <c:if
									test="${userInfo.name == user.name}">
									<a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
								</c:if></td>
						</c:if>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>