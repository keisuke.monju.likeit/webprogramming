<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>ユーザー削除画面</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>
	<header>
  				<form style="text-align: right" action="LogoutServlet"
			method="get">
				<input type="hidden" name="id" value="${user.id}">
			<input type="submit" class="btn btn-link" value="ログアウト">
				</form>
	</header>

	<div class="container">

		<p style="text-align: center">
			<font size="6">ユーザ削除詳細</font>
		</p>
	</div>
	<div class="sentence-area">
	ログインID：${user.loginId}
	</div>
	<div class="sentence-area">
	を本当に削除してよろしいでしょうか？
	</div>
	<div class="button-position row justify-content-center">
			<form action="ListServlet" method="get">
			<input type="submit" class="btn btn-secondary" value="キャンセル">
			</form>
			<form action="DeleteServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">
			<input type="submit" class="btn btn-danger" value="OK">
			</form>
	</div>
</body>
</html>