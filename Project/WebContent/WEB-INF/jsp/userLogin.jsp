<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ログイン画面</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<p style="text-align: center">
		<font size="7">ログイン画面</font>
	</p>
	<hr>

	<form class="login-area" action="LoginServlet" method="post">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
		<div class="form-group">
			<label for="exampleDropdownFormEmail2">ログインID</label>
			<input
				type="text" class="form-control" id="exampleDropdownFormEmail2"
				placeholder="RoginID" name="Loginid">
		</div>
		<div class="form-group">
			<label for="exampleDropdownFormPassword2">パスワード</label>
			<input
				type="password" class="form-control"
				id="exampleDropdownFormPassword2" placeholder="Password" name="password">
		</div>
		<div class="form-group">
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="dropdownCheck2">
				<label class="form-check-label" for="dropdownCheck2">
					Remember me </label>
			</div>
		</div>
		<input type="submit" class="btn btn-primary" value="ログイン">
	</form>
</body>
</html>